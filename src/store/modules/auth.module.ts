import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import AuthService from '../../services/AuthService';
import bus from '../../services/eventBusService';
import router from '../../router/index';

const storedUser = localStorage.getItem('user');

@Module({ namespaced: true })
class User extends VuexModule {
  public status = storedUser ? { loggedIn: true } : { loggedIn: false };
  public user = storedUser ? JSON.parse(storedUser) : null;
  public membershipStatus = '';

  @Mutation
  public loginSuccess(user: any): void {
    this.status.loggedIn = true;
    this.user = user;
  }

  @Mutation
  public loginFailure(): void {
    this.status.loggedIn = false;
    this.user = null;
  }

  // @Mutation
  // public setMembership(membership: any): void {
  //   this.membershipStatus = membership;
  // }

  @Mutation
  public logout(): void {
    this.status.loggedIn = false;
    this.user = null;
  }

  @Mutation
  public registerSuccess(): void {
    this.status.loggedIn = false;
  }

  @Mutation
  public registerFailure(): void {
    this.status.loggedIn = false;
  }

  @Action({ rawError: true })
  async login(data: any): Promise<any> {
    return AuthService.login(data.email, data.password).then(
      user => {
        console.log(user);
        this.context.commit('loginSuccess', user);
        this.context.commit('setMembership', user.data.staff.membership_status);
        router.push({ name: 'dashboard' }).catch(err => {});
        // if(user.data.staff.membership_status == "new"){
        //   router.push({ name: 'user_verify' }).catch(err => {});
        // }else{
        //   router.push({ name: 'dashboard' }).catch(err => {});
        // }
        // bus.$emit('navigate', 'dashboard');
        // router.push({ name: 'dashboard' }).catch(err => {});
        // return Promise.resolve(user);
      },
      error => {
        this.context.commit('loginFailure');
        const message =
          (error.response && error.response.data && error.response.data.message) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  }

  // @Action
  // signOut(): void {
  //   AuthService.logout();
  //   this.context.commit('logout');
  // }

  @Action({ rawError: true })
  register(data: any): Promise<any> {
    return AuthService.register(data.username, data.email, data.password).then(
      response => {
        this.context.commit('registerSuccess');
        return Promise.resolve(response.data);
      },
      error => {
        this.context.commit('registerFailure');
        const message =
          (error.response && error.response.data && error.response.data.message) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  }

  get isLoggedIn(): boolean {
    return this.status.loggedIn;
  }
}

export default User;
