import Vue from 'vue';
import App from './App.vue';
import router from './router/index';
import bus from './services/eventBusService';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VeeValidate from 'vee-validate';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import JsonExcel from "vue-json-to-excel/JsonExcel.vue";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import {
  faHome,
  faBars,
  faTruck,
  faTrailer,
  faBookReader,
  faTasks,
  faFileInvoice,
  faHandshake,
  faMoneyCheckAlt,
  faCashRegister,
  faMoneyBill,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faUsers,
  faPlus,
  faPlusCircle,
  faFileSignature,
  faPenSquare,
  faPen,
  faTrash,
  faUserLock,
  faBan,
  faLock,
  faLockOpen,
  faBookmark,
  faCog,
  faCheck,
  faFile,
  faCross,
  faTimes,
  faEye,
  faArrowLeft,
  faArrowRight,
  faArrowUp,
  faArrowDown,
  faEdit,
  faTrashAlt,
  faUserCog,
  faUserTie,
  faAngleLeft,
  faAngleRight,
  faSearch,
  faCar,
  faCogs,
  faUniversity,
  faCreditCard,
  faWrench,
  faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';
// import vueEventManager from './folder/vueeventmanager';
// window.Event = new vueEventManager();
import axios from 'axios';
import $ from 'jquery';


axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


import * as UIkit from 'uikit';
 
Vue.component("downloadExcel", JsonExcel);

// import * as Icons from 'uikit/dist/js/uikit-icons.js';
// UIkit.use(Icons);

library.add(
  faHome,
  faCreditCard,
  faBars,
  faTruck,
  faTrailer,
  faBookReader,
  faTasks,
  faFileInvoice,
  faHandshake,
  faMoneyCheckAlt,
  faCashRegister,
  faMoneyBill,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faUsers,
  faPlus,
  faPlusCircle,
  faFileSignature,
  faPenSquare,
  faPen,
  faTrash,
  faBookmark,
  faUserLock,
  faBan,
  faLock,
  faLockOpen,
  faCog,
  faCheck,
  faFile,
  faCross,
  faTimes,
  faEye,
  faArrowLeft,
  faArrowRight,
  faArrowUp,
  faArrowDown,
  faEdit,
  faTrashAlt,
  faUserCog,
  faUserTie,
  faAngleLeft,
  faAngleRight,
  faSearch,
  faCar,
  faCogs,
  faUniversity,
  faWrench,
    faMapMarkerAlt,
);

// Vue.directive('tooltip', function(el, binding){
//   $(el).tooltip({
//            title: binding.value,
//            placement: binding.arg,
//            trigger: 'hover'             
//        })
// })

Vue.config.productionTip = false;

Vue.use(VeeValidate);
Vue.use(VueSweetalert2);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  bus,
  el: '#app',
  render: h => h(App)
}).$mount('#app');
