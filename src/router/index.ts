import Vue from 'vue';
import VueRouter, { RouteConfig } from "vue-router";
import Home from '@/components/Home.vue';
import Login from '../components/auth/Login.vue';
import Dashboard from '../components/Landing.vue';
import Layout from '../components/layout/layout.vue';
import app from '../App.vue'
// import EventBus from 'vue-bus-ts';

import VerifyUserState from '../services/UserService';
import { TokenService } from '../services/StorageService';
// import Authorize  from '../services/AuthorizationService';
import AuthAlert  from '../services/authAlert';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: {
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Layout,
    meta: {
      public: false,
      profileCompleteCheck: true,
      authRequired: true
    },
    children: [
      {
        path: '/verify/User',
        name: 'user_verify',
        component: () => import( '../components/auth/VerifyPassword.vue'),
      },
      {
        path: '/home',
        name: 'dashboard',
        component: () => import( '../components/Landing.vue'),
      },
      
      // {
      //   path: '/home',
      //   name: 'home',
      //   component: () => import( '../components/Home.vue')
      // },
      {
        path: '/booking',
        name: 'booking',
        component: () => import( '../components/sections/Booking.vue')
      },
      {
        path: '/employees',
        name: 'employees',
        component: () => import( '../components/sections/Employees.vue')
      },
      {
        path: '/payroll',
        name: 'payroll',
        component: () => import( '../components/sections/payroll/Payroll.vue')
      },
      {
        path: '/loans',
        name: 'loans',
        component: () => import( '../components/sections/payroll/loans.vue')
      },
      { 
        path:'/loan_details/:id', 
        name: 'loan_details', 
        props: true , 
        component: () => import('../components/sections/payroll/EmployeeLoanInfo.vue')
      },
      {
        path: '/taxes',
        name: 'taxes',
        component: () => import( '../components/sections/payroll/taxAndInsurance.vue')
      },
      {
        path: '/roles',
        name: 'roles',
        component: () => import( '../components/sections/Roles.vue')
      },
      {
        path: '/departments',
        name: 'department',
        component: () => import( '../components/sections/Department.vue')
      },
      {
        path: '/home/request',
        name: 'request',
        component: () => import( '../components/sections/Request.vue')
      },
      {
        path: '/requests',
        name: 'requests',
        component: () => import( '../components/sections/requests/requestList.vue')
      },
      {
        path: '/requests/office',
        name: 'office',
        component: () => import( '../components/sections/requests/Office.vue')
      },
      {
        path: '/requests/fuel',
        name: 'fuel',
        component: () => import( '../components/sections/requests/Fuel.vue')
      },
      {
        path: '/new/requests/fuel',
        name: 'fuel_register',
        component: () => import( '../components/sections/req/Fuel.vue')
      },
      {
        path: '/new/requests/office',
        name: 'office_requests',
        component: () => import( '../components/sections/req/Office.vue')
      },
      {
        path: '/requests/expenditure',
        name: 'expenditure',
        component: () => import( '../components/sections/requests/Expenditure.vue')
      },
      {
        path: '/requests/trailer',
        name: 'trailer',
        component: () => import( '../components/sections/requests/Trailer.vue')
      },
      {
        path: '/requests/horse',
        name: 'horse',
        component: () => import( '../components/sections/requests/Horse.vue')
      },
      {
        path: '/requests/vehicle',
        name: 'vehicle',
        component: () => import( '../components/sections/requests/Vehicle.vue')
      },
      {
        path: '/sales',
        name: 'sales',
        component: () => import( '../components/sections/Sales.vue')
      },
      {
        path: '/purchase',
        name: 'purchase',
        component: () => import( '../components/sections/Purchase.vue')
      },
      {
        path: '/management',
        name: 'management',
        component: () => import( '../components/Management/Management.vue')
      },
      {
        path: '/management/store',
        name: 'store',
        component: () => import( '../components/Management/store/store.vue')
      },
      {
        path: '/management/store/vehicle',
        name: 'vehicles',
        component: () => import( '../components/Management/store/Vehicles.vue')
      },
      {
        path: '/management/store/trailer',
        name: 'trailers',
        component: () => import( '../components/Management/store/Trailers.vue')
      },
      {
        path: '/management/store/horse',
        name: 'horses',
        component: () => import( '../components/Management/store/Horses.vue')
      },
      {
        path: '/management/store/insurance',
        name: 'insurance',
        component: () => import( '../components/Management/store/Insurance.vue')
      },
      {
        path: '/management/security',
        name: 'security',
        component: () => import( '../components/Management/security/Security.vue')
      },
      {
        path: '/management/security/authorization',
        name: 'authorization',
        component: () => import( '../components/Management/security/Privileges.vue')
      },
      {
        path: '/management/security/logs',
        name: 'logs',
        component: () => import( '../components/Management/security/SysLogs.vue')
      },
      {
        path: '/management/security/users',
        name: 'user_security',
        component: () => import( '../components/Management/users/users.vue')
      },
      { 
        path:'/cargo_details/:id', 
        name: 'cargo_details', 
        props: true , 
        component: () => import('../components/sections/CargoDetails.vue')
      },
      { 
        path:'/booking_payments/:id', 
        name: 'booking_payments', 
        props: true , 
        component: () => import('../components/sections/BookingPayment.vue')
      },
      {
        path:'/debtors',
        name: 'debtors',
        component: () => import('../components/sections/accountant/Debtors.vue')
      },
      {
        path:'/creditors',
        name: 'creditors',
        component: () => import('../components/sections/accountant/Creditors.vue')
      },

      {
        path:'/new/horse/registration',
        name: 'horse_registration',
        component: () => import('../components/sections/horse/Horse.vue')
      },

      {
        path:'/new/trailer/registration',
        name: 'trailer_registration',
        component: () => import('../components/sections/horse/Trailer.vue')
      },

      {
        path:'/new/vehicle/registration',
        name: 'vehicle_registration',
        component: () => import('../components/sections/horse/Vehicle.vue')
      },

      {
        path:'/show/horse/details/:id',
        name: 'horse_details',
        props: true ,
        component: () => import('../components/sections/horse/HorseView.vue')
      },

      {
        path:'/show/vehicle/details/:id',
        name: 'vehicle_details',
        props: true ,
        component: () => import('../components/sections/horse/VehicleView.vue')
      },

      { 
        path:'/invoice/:id', 
        name: 'invoice', 
        props: true , 
        component: () => import('../components/sections/accountant/Invoice.vue')
      },
      { 
        path:'/setting', 
        name: 'setting', 
        component: () => import('../components/sections/setting/Setting.vue')
      },

      {
        path:'/user/profile',
        name: 'profile',
        component: () => import('../components/sections/Profile.vue')
      },

    ]
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  // const bus = new EventBus.Bus();
  const isPublic = to.matched.some(record => record.meta.public)
  const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut)
  const loggedIn = TokenService.getToken();
  // const userState = VerifyUserState.verify();
  // console.log(userState);
  // const isAllowed = Authorize.authorizeViewRoute(to.name);
  if (!isPublic && !loggedIn) {
    return next({
      path: '/',
    });
  }
  // if (!userState) {
  //   return next({
  //     path: 'user/verify',
  //   });
  // }

  
  next();
})

export default router
