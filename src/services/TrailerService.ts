import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class TrailerService {
  getAll() {

    return axios
      .get(API.local() + 'trailerRequests/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }

  get(id: string) {
    return http.get(`/horse/${id}`);
  }

  create(trailer: any) {
    return axios
    .post(API.local() + 'trailerRequest/create/'+ user.staff.company_id, trailer
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  update(id: string, trailer: any) {
    return axios
        .post(API.local() + 'trailerRequest/update/'+ id, trailer
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }
  delete(id: string) {
      return axios
        .get(API.local() + 'trailerRequest/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }



  
}

export default new TrailerService();
