export default function authHeader() {
    const storedUser = localStorage.getItem('user');
    const user = JSON.parse(storedUser ? storedUser : "");
  
    if (user && user.accessToken) {
      return { 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      };
    } else {
      return {};
    }
  }