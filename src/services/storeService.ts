import axios from 'axios';
import authHeader from './auth-header';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/store/';

class storeService {
  getHorse() {
    return axios
      .get(API.local() + 'get/horse', { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  getTrailer() {
    return axios
      .get(API.local() + 'get/horse', { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  createHorse(horse: any) {
    return axios
      .post(API.local() + 'create', horse,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  createTrailer(trailer: any) {
    return axios
      .post(API.local() + 'create', trailer,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  updateHorse(horse: any) {
    return axios
      .put(API.local() + 'update', horse,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  updateTrailer(trailer: any) {
    return axios
      .put(API.local() + 'update', trailer,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  assignHorse(horse: any){
    return axios
    .put(API.local() + 'update/'+ horse,{ headers: authHeader() })
    .then(response => {
      return response.data;
    });
  }

  assignTrailer(horse: any){
    return axios
    .put(API.local() + 'update/'+ horse,{ headers: authHeader() })
    .then(response => {
      return response.data;
    });
  }

  deleteHorse(horse: string) {
    return axios
      .delete(API.local() + 'delete/'+ horse, { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  deleteTrailer(trailer: string) {
    return axios
      .delete(API.local() + 'delete/'+ trailer, { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }



  
}

export default new storeService();
