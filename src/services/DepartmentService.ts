// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/expenditure/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://localhost:8000/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class DepartmentService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  getAll() {
    return axios
      .get(API.local() + 'departments/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/expenditures");
  }

  get(id: string) {
    return http.get(`/department/${id}`);
  }

  create(department: any) {
    return axios
    .post(API.local() + 'department/create/' + user.staff.company_id, department
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/expenditure/create", expenditure);
  }

  // update(expenditure: any) {
  //   return axios
  //     .put(API_URL + 'update', expenditure,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, department: any) {
    return axios
        .post(API.local() + 'department/update/'+ id, department
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/expenditure/update/'+ id, expenditure);
  }
  delete(id: string) {
    return axios
      .get(API.local() + 'department/delete/'+ id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
    )
    .then(response => {
      return response.data;
    });
      // return http.delete('/expenditure/delete/' + id);
  }



  
}

export default new DepartmentService();
