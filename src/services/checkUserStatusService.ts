import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';

const API_URL = 'http://localhost:8000/api/';
import API from './aPILink';

class AuthUserStatusService {
  

  auth() {
    const storedUser = localStorage.getItem('user');
    const user = JSON.parse(storedUser ? storedUser : "");
    return user.status == "new";
  }
}

export default new AuthUserStatusService();