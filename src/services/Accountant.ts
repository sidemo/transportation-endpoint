import axios from "axios";
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class AccountantService {
    AllDebtors(){
        return axios.get( API.local() + 'show/debtors/list/' + user.staff.company_id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
        ).then(response => {
            console.log(response.data)
            return response.data;
        });
    }

    AllCreditors(){
        return axios.get( API.local() + 'show/creditors/list/' + user.staff.company_id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
        ).then(response => {
            console.log(response.data)
            return response.data;
        });
    }

    InvoiceData(id: any){
        return axios.get( API.local() + 'show/invoice/details/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
        ).then(response => {
            return response.data;
        });
    }

}

export default new AccountantService()
