import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class HorseStoreService {
  getAll() {

    return axios
      .get(API.local() + 'horses/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
  }

  get(id: string) {
    return http.get(`/horse/${id}`);
  }

  create(horse: any) {
    return axios
    .post(API.local() + 'horse/create/'+ user.staff.company_id , horse
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  update(id: string, horse: any) {
    return axios
        .post(API.local() + 'horse/update/'+ id + '/' + user.staff.company_id, horse
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/horse/update/'+ id, horse);
  }
  delete(id: string) {
      // return http.delete('/horse/delete/' + id);
      return axios
        .get(API.local() + 'horse/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }



  
}

export default new HorseStoreService();
