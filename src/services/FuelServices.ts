// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/fuel/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class FuelService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  getAll() {
    return axios
      .get(API.local() + 'fuels/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/fuels");
  }

  get(id: string) {
    return http.get(`/fuel/${id}`);
  }

  create(fuel: any) {
    return axios
    .post(API.local() + 'fuel/create/' + user.staff.company_id, fuel
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/fuel/create", fuel);
  }

  // update(fuel: any) {
  //   return axios
  //     .put(API_URL + 'update', fuel,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, fuel: any) {
    return axios
        .post(API.local() + 'fuel/update/'+ id, fuel
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/fuel/update/'+ id, fuel);
  }
  delete(id: string) {
    return axios
      .get(API.local() + 'fuel/delete/'+ id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
    )
    .then(response => {
      return response.data;
    });
      // return http.delete('/fuel/delete/' + id);
  }



  
}

export default new FuelService();
