import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class HorseService {

  getAll() {

    return axios
      .get(API.local() + 'horseRequests/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
  }

  get(id: string) {
    return http.get(`/horse/${id}`);
  }

  create(horse: any) {
    return axios
    .post(API.local() + 'horseRequest/create/'+ user.staff.company_id , horse
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  update(id: string, horse: any) {
    return axios
        .post(API.local() + 'horseRequest/update/'+ id + '/' + user.staff.company_id, horse
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/horse/update/'+ id, horse);
  }
  delete(id: string) {
      // return http.delete('/horse/delete/' + id);
      return axios
        .get(API.local() + 'horseRequest/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }

  ShowRegisteredHorse(){
      return axios
          .get(API.local() + 'view/register/new/horse/'+ user.staff.company_id
              ,{headers:{
                      'Content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'accessToken':  user.accessToken,
                      'staff_id': user.staff.id
                  }
              }
          )
          .then(response => {
              return response.data;
          });
  }

  RegisterHorse(Horse: any){
      // console.log(Horse);
      return axios.post(API.local() + 'register/new/horse/' + user.staff.company_id, Horse,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
          ).then(response => {
          return response.data;
      });
  }

  EditRegisteredHorse(id:any, data:any){
      return axios
          .post(API.local() + 'edit/register/horse/'+ id , data
              ,{headers:{
                      'Content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'accessToken':  user.accessToken,
                      'staff_id': user.staff.id
                  }
              }
          )
          .then(response => {
              return response.data;
          });
  }

  DeleteRegisteredHorse(id: string){
      return axios
          .get(API.local() + 'delete/register/horse/'+ id
              ,{headers:{
                      'Content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'accessToken':  user.accessToken,
                      'staff_id': user.staff.id
                  }
              }
          )
          .then(response => {
              return response.data;
          });
  }

  ViewRegisteredHorse(id: any){
      return axios.get( API.local() + 'show/register/horse/details/' + id,
          {headers: {
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }}
      ).then(response => {
          return response.data
      })
  }

  viewHorseInsurance(id: any){
      return axios.get( API.local() + 'show/register/horse/insurance/' + id,
          {headers: {
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }}
      ).then(response => {
          return response.data
      })
  }

  newHorseInsurance(id:any, insurance: any){
      return axios.post(API.local() + 'add/insurance/to/register/horse/' + id, insurance,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  NewRoute(id: any, data:any){
      return axios.post(API.local() + 'add/route/to/register/horse/' + id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  NewService(id: any, data: any){
      return axios.post(API.local() + 'add/service/to/register/horse/' + id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  NewDriver(id: any, data: any){
      return axios.post(API.local() + 'assign/driver/to/horse/' + id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  newVehicleDriver(id: any, data: any){
      return axios.post(API.local() + 'assign/driver/to/vehicle/' + id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  viewRegisteredVehicles(){
      return axios.get( API.local() + 'show/register/vehicle/' + user.staff.company_id,
          {headers: {
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }}
      ).then(response => {
          return response.data
      })
  }

  registerVehicle(data: any){
      return axios.post(API.local() + 'register/new/vehicle/' + user.staff.company_id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  updateRegisterVehicle(id: any, data: any){
      return axios
          .post(API.local() + 'edit/vehicle/'+ id , data
              ,{headers:{
                      'Content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'accessToken':  user.accessToken,
                      'staff_id': user.staff.id
                  }
              }
          )
          .then(response => {
              console.log(response.data)
              return response.data;
          });
  }

  deletedRegisteredVehicle(id: string){
      return axios
          .get(API.local() + 'delete/register/vehicle/'+ id
              ,{headers:{
                      'Content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'accessToken':  user.accessToken,
                      'staff_id': user.staff.id
                  }
              }
          )
          .then(response => {
              return response.data;
          });
  }

  showRegisteredVehicle(id: any){
      return axios.get( API.local() + 'view/register/vehicle/' + id,
          {headers: {
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }}
      ).then(response => {
          return response.data
      })
  }

  VehicleInsurance(id:any, insurance: any){
      return axios.post(API.local() + 'vehicle/add/insurance/' + id, insurance,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  vehicleService(id: any, data: any){
      return axios.post(API.local() + 'add/service/to/vehicle/' + id, data,
          {headers:{
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }
          }
      ).then(response => {
          return response.data;
      });
  }

  viewDriversList(){
      return axios.get( API.local() + 'view/driver/list/' + user.staff.company_id,
          {headers: {
                  'Content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  'accessToken':  user.accessToken,
                  'staff_id': user.staff.id
              }}
      ).then(response => {
          return response.data
      })
  }
}

export default new HorseService();
