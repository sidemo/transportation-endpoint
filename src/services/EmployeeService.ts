import axios from 'axios';
import authHeader from './auth-header';
import API from './aPILink';
import http from '../http-common';

const API_URL = 'http://localhost:8000/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class EmployeeService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }
  updatePassword(email: string, entered_password: string, new_password: string){
    return axios
      .post(API.local() + 'update/password', {
        email,
        entered_password,
        new_password
      }
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
  }
  
  getAll() {
    // return http.get("/employees/");
    return axios
      .get(API.local() + 'employees/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
  }

  get(id: string) {
    return http.get('/employee/'+ id);
  }

  create(employee: any) {
    return axios
      .post(API.local() + 'employee/create/'+ user.staff.company_id, employee
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.post("/create/employee/", employee);
  }

  // update(employee: any) {
  //   return axios
  //     .put(API_URL + 'update', employee,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, employee: any) {
    return axios
      .post(API.local() + 'employee/update/' + id +'/'+ user.staff.company_id, employee
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
    });
    // return http.put('/employee/update/'+ id, employee);
  }

  changeEmployeeSystemUsing(id: string, employee: any) {
    return axios
      .post(API.local() + 'employee/change/system/access/' + id, employee
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
    });
    return http.put('/employee/change/system/access/'+ id, employee);
  }

  delete(id: string) {
      return http.delete('/employee/delete/' + id);
  }



  
}

export default new EmployeeService();
