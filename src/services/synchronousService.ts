class synchronizeOperation {
    setBoolean(_state:boolean){
        var state;
        if(_state){
            setTimeout(function(){
                state = false;
            }.bind(this), 3000)
        }else{
            setTimeout(function(){
                state = true;
            }.bind(this), 3000)
        }
        return state;
    }

    Await(_state:boolean, _time: any){
        var state;
        if(_state){
            setTimeout(function(){
                state = false;
            }.bind(this), _time)
        }else{
            setTimeout(function(){
                state = true;
            }.bind(this), _time)
        }
        return state;
    }
}
export default new synchronizeOperation();
