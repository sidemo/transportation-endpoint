
class AuthorizationStorage {
    private userRole: any = {};
    private userAuthorization: any = [];
    public authObjects: any = {};
    private privileges: any = {};
    public Allowed: boolean = false;
    private authIndex = "";
    private permissionIndex = "";
    private authkey = "";
    private permission: any = {};

    getAuthObjects(){
        this.authObjects = {
            booking_payments: "Booking",
            dashboard: "Company",
            booking: "Booking",
            employees: "Employee",
            payroll: "Payroll",
            loans: "Payroll",
            loan_details: "Payroll",
            taxes: "Payroll",
            roles: "Role",
            departments: "Department",
            trailer: "Trailer Request",
            horse: "Horse Request",
            expenditure: "Expenditure Request",
            office: "Office Request",
            fuel: "Fuel Request",
            vehicle: "Vehicle Request",
            vehicles: "Vehicle",
            horses: "Horse",
            trailers: "Trailer",
            requests: "Request",
            sales: "Sales",
            accountant: "Sales",
            insurance: "Insurance",
            purchase: "Purchase",
            management: "Company",
            setting: "Company",
            store: "Company",
            security: "Permission",
            authorization: "Permission",
            logs: "Permission",
            cargo_details: "Cargo",
            user_security: "Permission",
    
        };
    }

    authorizeViewRoute(route: any){
        this.permission = "";
        this.permissionIndex = "";
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(route == "login" || route == "dashboard" || route == "home"){
            return true;
        }else{
            this.getAuthObjects();
            var authKeys = Object.keys(this.authObjects);
            for(var i in authKeys){
                if(authKeys[i] == route){
                    this.authIndex = i;
                    this.authkey = authKeys[i];
                    break;
                }
            }
            if(this.privileges != "" && this.userRole != ""){
                for(var i in this.privileges){
                    if(this.privileges[i].permission == this.authObjects[this.authkey] && this.privileges[i].role_id == this.userRole.id){
                        this.permission = this.privileges[i];
                        this.permissionIndex = i;
                    }
                }
            }
            if(this.permission.view){
                return true;
            }else{
                return false;
            }
        }
    }

    authorizeCreateRoute(route: any){
        this.permission = "";
        this.permissionIndex = "";
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(route == "login" || route == "dashboard" || route == "home"){
            return true;
        }else{
            this.getAuthObjects();
            var authKeys = Object.keys(this.authObjects);
            for(var i in authKeys){
                if(authKeys[i] == route){
                    this.authIndex = i;
                    this.authkey = authKeys[i];
                    break;
                }
            }
            
            if(this.privileges != "" && this.userRole != ""){
                for(var i in this.privileges){
                    if(this.privileges[i].permission == this.authObjects[this.authkey] && this.privileges[i].role_id == this.userRole.id){
                        this.permission = this.privileges[i];
                        this.permissionIndex = i;
                    }
                }
            }
            if(this.permission.create){
                return true;
            }else{
                return false;
            }
        }
    }

    authorizeUpdateRoute(route: any){
        this.permission = "";
        this.permissionIndex = "";
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(route == "login" || route == "dashboard" || route == "home"){
            return true;
        }else{
            this.getAuthObjects();
            var authKeys = Object.keys(this.authObjects);
            for(var i in authKeys){
                if(authKeys[i] == route){
                    this.authIndex = i;
                    this.authkey = authKeys[i];
                    break;
                }
            }
            
            if(this.privileges != "" && this.userRole != ""){
                for(var i in this.privileges){
                    if(this.privileges[i].permission == this.authObjects[this.authkey] && this.privileges[i].role_id == this.userRole.id){
                        this.permission = this.privileges[i];
                        this.permissionIndex = i;
                    }
                }
            }
            if(this.permission.update){
                return true;
            }else{
                return false;
            }
        }
    }

    authorizeDeleteRoute(route: any){
        this.permission = "";
        this.permissionIndex = "";
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(route == "login" || route == "dashboard" || route == "home"){
            return true;
        }else{
            this.getAuthObjects();
            var authKeys = Object.keys(this.authObjects);
            for(var i in authKeys){
                if(authKeys[i] == route){
                    this.authIndex = i;
                    this.authkey = authKeys[i];
                    break;
                }
            }
            if(this.privileges != "" && this.userRole != ""){
                for(var i in this.privileges){
                    if(this.privileges[i].permission == this.authObjects[this.authkey] && this.privileges[i].role_id == this.userRole.id){
                        this.permission = this.privileges[i];
                        this.permissionIndex = i;
                    }
                }
            }
            if(this.permission.delete){
                return true;
            }else{
                return false;
            }
        }
    }

    authorizeManageRoute(route: any){
        this.permission = "";
        this.permissionIndex = "";
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(route == "login" || route == "dashboard" || route == "home"){
            return true;
        }else{
            this.getAuthObjects();
            var authKeys = Object.keys(this.authObjects);
            for(var i in authKeys){
                if(authKeys[i] == route){
                    this.authIndex = i;
                    this.authkey = authKeys[i];
                    break;
                }
            }
            if(this.privileges != "" && this.userRole != ""){
                for(var i in this.privileges){
                    if(this.privileges[i].permission == this.authObjects[this.authkey] && this.privileges[i].role_id == this.userRole.id){
                        this.permission = this.privileges[i];
                        this.permissionIndex = i;
                    }
                }
            }
            if(this.permission.manage){
                return true;
            }else{
                return false;
            }
        }
    }

    authRequests(route: any){
        this.authorizeViewRoute(route);
        const storedRole = localStorage.getItem('role');
        const role = JSON.parse(storedRole ? storedRole : "");
        const storedPrivilege = localStorage.getItem('auth');
        const privilege = JSON.parse(storedPrivilege ? storedPrivilege : "");
        this.userRole = role;
        this.privileges = privilege;
        if(this.userRole.name == 'Managing Director'){
            return true;
        }else{
            return false;
        }
    }

  privilegeInformation(roles: any, privileges: any) {
    for(var i in roles){
        for(var j in privileges){
            if(privileges[j].role_id == roles[i].id){
                if(parseInt(privileges[j].auth)%2 == 0){
                    privileges[j].view = true;
                }else{
                    privileges[j].view = false;
                }
                if(parseInt(privileges[j].auth)%3 == 0){
                    privileges[j].create = true;
                }else{
                    privileges[j].create = false;
                }
                if(parseInt(privileges[j].auth)%5 == 0){
                    privileges[j].update = true;
                }else{
                    privileges[j].update = false;
                }
                if(parseInt(privileges[j].auth)%7 == 0){
                    privileges[j].delete = true;
                }else{
                    privileges[j].delete = false;
                }
                if(parseInt(privileges[j].auth)%11 == 0){
                    privileges[j].manage = true;
                }else{
                    privileges[j].manage = false;
                }
            }
            
        }
    }

    return privileges;
  }

  privilegeDataStorage(roles: any, privileges: any){
    for(var i in roles){
        for(var j in privileges){
            if(privileges[j].role_id == roles[i].id){
                if(parseInt(privileges[j].auth)%2 == 0){
                    privileges[j].view = true;
                }else{
                    privileges[j].view = false;
                }
                if(parseInt(privileges[j].auth)%3 == 0){
                    privileges[j].create = true;
                }else{
                    privileges[j].create = false;
                }
                if(parseInt(privileges[j].auth)%5 == 0){
                    privileges[j].update = true;
                }else{
                    privileges[j].update = false;
                }
                if(parseInt(privileges[j].auth)%7 == 0){
                    privileges[j].delete = true;
                }else{
                    privileges[j].delete = false;
                }
                if(parseInt(privileges[j].auth)%11 == 0){
                    privileges[j].manage = true;
                }else{
                    privileges[j].manage = false;
                }
            }
            
        }
    }
    localStorage.setItem('auth', JSON.stringify(privileges));
  }

  
  
}

export default new AuthorizationStorage();