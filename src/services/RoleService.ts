// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/role/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class RoleService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  getAll() {
    return axios
      .get(API.local() + 'roles/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/roles");
  }

  get(id: string) {
    return http.get(`/role/${id}`);
  }

  create(role: any) {
    return axios
    .post(API.local() + 'role/create/'+ user.staff.company_id, role
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/role/create", role);
  }

  // update(role: any) {
  //   return axios
  //     .put(API_URL + 'update', role,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, role: any) {
    return axios
        .post(API.local() + 'horse/role/'+ id, role
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/role/update/'+ id, role);
  }
  delete(id: string) {
    return axios
        .get(API.local() + 'role/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
      // return http.delete('/role/delete/' + id);
  }



  
}

export default new RoleService();
