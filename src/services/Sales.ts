import axios from "axios";
import http from '../http-common';
import index from "@/store";
// import http from '../http-common';
// const API_URL = 'https://portal.ratco.co.tz/api/';
const API_URL = 'http://localhost:8000/api/';
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class SalesService {

    AllCargo() {
        return axios.get( API.local() + 'show/all/cargos/' + user.staff.company_id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
            return response.data;
        });
    }

     SaveCargo(cargo: any) {
        console.log(cargo)
        return axios.post(API.local() + 'create/cargo/'+ user.staff.company_id, cargo,
            {headers:{
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }
            }).then(response => {
                return response.data;
        });
    }

    editCargo(id: any, cargo: any){
        return axios.post( API.local() + 'update/cargo/' + id, cargo,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
                return response.data;
        })
    }

    deleteCargo(id: string) {
        return axios .get( API.local() + 'delete/cargo/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }})
            .then(response => {
                return response.data;
            });
    }

    SaveBooking(booking: any, id:any){
        return axios.post( API.local() + 'create/booking/' + id, booking,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
                return response.data
        });
    }

    EditBooking(booking: any, id: any){
        return axios.post( API.local() + 'update/booking/' + id, booking,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
        ).then(response => {
            return response.data
        });
    }

    deleteBooking(id: any){
        return axios.get( API.local() + 'delete/booking/' + id,{
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'accessToken':  user.accessToken,
                'staff_id': user.staff.id
            }
        } ).then(response => {
            return response.data
        });
    }

    viewCargo(id: any){
        return axios .get( API.local() + 'view/cargo/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }} ).then(response => {
                    return response.data;
        })
    }

    viewCargoBooking(id: any) {
        return axios.get( API.local() + 'view/booking/belong/to/cargo/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
                return response.data
        })
    }

    viewBooking(id: any){
        return axios.get( API.local() + 'view/booking/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}).then(response => {
                    return response.data
        })
    }

    viewBookingPayments(id: any){
        return axios.get( API.local() + 'view/payments/booking/' + id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
            return response.data
        })
    }

    saveBookingPayments(id: any, payment: any){
        return axios.post( API.local() + 'create/booking/payment/' + id, payment,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}).then(response => {
                    console.log(response)
                    return response.data
                })
            }

}

export default new SalesService();
