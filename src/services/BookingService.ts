// import axios from 'axios';
// import authHeader from './auth-header';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/booking/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");


class BookingService {
  get() {
    return axios
      .get(API.local() + 'get', { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  getAll() {
    return axios
      .get(API.local() + 'bookings/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/expenditures");
  }

  create(booking: any) {
    return axios
    .post(API.local() + 'booking/create/'+ user.staff.company_id , booking
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return axios
    //   .post(API_URL + 'create', booking,{ headers: authHeader() })
    //   .then(response => {
    //     return response.data;
    //   });

      
  }

  
  update(id: string, booking: any) {
    return axios
        .post(API.local() + 'expenditure/update/'+ id, booking
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/expenditure/update/'+ id, expenditure);
  }

  changeEmployeeSystemUsing(booking: any) {
    return axios
      .put(API.local() + 'revoke/system/using', booking,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

  delete(booking: string) {
    return axios
      .delete(API.local() + 'delete/'+ booking, { headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }



  
}

export default new BookingService();
