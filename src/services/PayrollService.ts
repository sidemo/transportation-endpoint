import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';

const API_URL = 'http://localhost:8000/api/';
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class PayrollService {
  getAllTaxes() {

    return axios
      .get(API.local() + 'taxAndInsurances/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }


  createTaxes(taxAndDebt: any) {
    return axios
    .post(API.local() + 'taxAndInsurance/create/'+ user.staff.company_id, taxAndDebt
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  updateTaxes(id: string, taxAndDebt: any) {
    return axios
        .post(API.local() + 'taxAndInsurance/update/'+ id + '/'+ user.staff.company_id, taxAndDebt
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }
  deleteTaxes(id: string) {
      return axios
        .get(API.local() + 'taxAndInsurance/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }




  getAllLoans() {

    return axios
      .get(API.local() + 'taxAndLoans/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }
  createLoans(taxAndDebt: any) {
    return axios
    .post(API.local() + 'taxAndLoan/create/'+ user.staff.company_id, taxAndDebt
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  updateLoans(id: string, taxAndDebt: any) {
    return axios
        .post(API.local() + 'taxAndLoan/update/'+ id + '/'+ user.staff.company_id, taxAndDebt
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }
  deleteLoans(id: string) {
      return axios
        .get(API.local() + 'taxAndLoan/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }

  getAllEmpLoans() {

    return axios
      .get(API.local() + 'employeeTaxAndLoans/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }
  createEmpLoans(taxAndDebt: any) {
    return axios
    .post(API.local() + 'employeeTaxAndLoan/create/'+ user.staff.company_id, taxAndDebt
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }

  updateEmpLoans(id: string, taxAndDebt: any) {
    return axios
        .post(API.local() + 'employeeTaxAndLoan/update/'+ id + '/'+ user.staff.company_id, taxAndDebt
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }

  deleteEmpLoans(id: string) {
      return axios
        .get(API.local() + 'employeeTaxAndLoan/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }


  getAllEmpTaxes() {

    return axios
      .get(API.local() + 'employeeTaxAndInsurances/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }


  createEmpTaxes(taxAndDebt: any) {
    return axios
    .post(API.local() + 'employeeTaxAndInsurance/create/'+ user.staff.company_id, taxAndDebt
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  
  deleteEmpTaxes(id: string) {
      return axios
        .get(API.local() + 'employeeTaxAndInsurance/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }

  getPayroll() {

    return axios
      .get(API.local() + 'payrolls/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }

  createPayroll(payroll: any) {
    return axios
    .post(API.local() + 'payroll/create/'+ user.staff.company_id, payroll
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }

  
}

export default new PayrollService();