// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/expenditure/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class ExpenditureService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  getAll() {
    return axios
      .get(API.local() + 'expenditures/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/expenditures");
  }

  get(id: string) {
    return http.get(`/expenditure/${id}`);
  }

  create(expenditure: any) {
    return axios
    .post(API.local() + 'expenditure/create/' + user.staff.company_id, expenditure
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/expenditure/create", expenditure);
  }

  // update(expenditure: any) {
  //   return axios
  //     .put(API_URL + 'update', expenditure,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, expenditure: any) {
    return axios
        .post(API.local() + 'expenditure/update/'+ id + "/" + user.staff.company_id, expenditure
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/expenditure/update/'+ id, expenditure);
  }
  delete(id: string) {
    return axios
      .get(API.local() + 'expenditure/delete/'+ id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
    )
    .then(response => {
      return response.data;
    });
      // return http.delete('/expenditure/delete/' + id);
  }



  
}

export default new ExpenditureService();
