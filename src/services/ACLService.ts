// import axios from 'axios';
// import authHeader from './auth-header';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/booking/';

import axios from 'axios';
import authHeader from './auth-header';
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");


class ACLService {

  getAll() {
    return axios
      .get(API.local() + 'view/privilege/configuration/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/expenditures");
  }

  authorize(permission: any) {
    return axios
    .post(API.local() + 'privilege/type/'+ user.staff.company_id , permission
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }

  authorizeAll(permission: any) {
    return axios
    .post(API.local() + 'privilege/all/'+ user.staff.company_id , permission
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }

  authorizeObject(permission: any) {
    return axios
    .post(API.local() + 'privilege/object/'+ user.staff.company_id , permission
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }

  

  
  update(id: string, booking: any) {
    return axios
        .post(API.local() + 'expenditure/update/'+ id, booking
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/expenditure/update/'+ id, expenditure);
  }

  changeEmployeeSystemUsing(booking: any) {
    return axios
      .put(API.local() + 'revoke/system/using', booking,{ headers: authHeader() })
      .then(response => {
        return response.data;
      });
  }

 



  
}

export default new ACLService();
