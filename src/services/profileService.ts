import axios from "axios";
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class ProfileService {
        userProfile(){
            return axios.get(API.local() + 'show/user/profile/' + user.staff.id,
                {headers: {
                        'Content-type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'accessToken':  user.accessToken,
                        'staff_id': user.staff.id
                    }}
                ).then(response => {
                return response.data;
            });
        }

        editProfile( details: any){
            return axios.post(API.local() + 'edit/employee/details/' +  user.staff.id, details,
                {headers: {
                        'Content-type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'accessToken':  user.accessToken,
                        'staff_id': user.staff.id
                    }}
                ).then(response => {
                return response.data;
            })
        }

        changePass(data: any){
            return axios.post(API.local() + 'edit/employee/password/' +  user.staff.id, data,
                {headers: {
                        'Content-type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'accessToken':  user.accessToken,
                        'staff_id': user.staff.id
                    }}
            ).then(response => {
                return response.data;
            })
        }

}

export default new ProfileService();
