// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/purchase/';

import axios from 'axios';
import authHeader from './auth-header';
import API from './aPILink';
import http from '../http-common';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class PurchaseService {
  getAll() {

    return axios
      .get(API.local() + 'purchases/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
  }

  get(id: string) {
    return http.get(`/purchase/${id}`);
  }

  create(purchase: any) {
    return axios
    .post(API.local() + 'purchase/create/'+ user.staff.company_id , purchase
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  update(id: string, purchase: any) {
    return axios
        .post(API.local() + 'purchase/update/'+ id + '/' + user.staff.company_id, purchase
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/purchase/update/'+ id, purchase);
  }
  delete(id: string) {
      // return http.delete('/purchase/delete/' + id);
      return axios
        .get(API.local() + 'purchase/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }



  
}

export default new PurchaseService();
