import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class InsuranceStoreService {
  getAll() {

    return axios
      .get(API.local() + 'insurances/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/horses");
  }

  get(id: string) {
    return http.get(`/horse/${id}`);
  }

  create(insurance: any) {
    return axios
    .post(API.local() + 'insurance/create/'+ user.staff.company_id, insurance
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
  }
  update(id: string, insurance: any) {
    return axios
        .post(API.local() + 'insurance/update/'+ id + '/'+ user.staff.company_id, insurance
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }
  delete(id: string) {
      return axios
        .get(API.local() + 'insurance/delete/'+ id 
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
  }



  
}

export default new InsuranceStoreService();
