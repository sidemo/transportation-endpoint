import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';

const API_URL = 'http://localhost:8000/api/';
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class RequestService {
 

  updateRequestStatus(request: any) {
    return axios
    .post(API.local() + 'request/change/status/' + user.staff.company_id, request
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/request/create", request);
  }




  
}

export default new RequestService();