import axios from "axios";
import http from '../http-common';
import index from "@/store";
// import http from '../http-common';
const API_URL = 'http://localhost:8000/api/';
import API from './aPILink';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class SettingServices {
    CompanyDetails(){
        return axios.get( API.local() + 'show/company/' +  user.staff.company_id,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }}
            ).then(response => {
            return response.data;
        });
    }

    editCompanyDetails(id: any, Details: any){
        return axios.post( API.local() + 'edit/company/details/' + id, Details,
            {headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'accessToken':  user.accessToken,
                    'staff_id': user.staff.id
                }} ).then(response => {
                    return response.data;
        });
    }

}

export default new SettingServices();
