// import axios from 'axios';
// import authHeader from './auth-header';
// import http from '../http-common';

// const API_URL = 'http://rts.portal.ratco.co.tz/api/office/';

import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class OfficeService {
  // get() {
  //   return axios
  //     .get(API_URL + 'get', { headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  getAll() {
    return axios
      .get(API.local() + 'offices/'+ user.staff.company_id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
      )
      .then(response => {
        return response.data;
      });
    // return http.get("/offices");
  }

  get(id: string) {
    return http.get(`/office/${id}`);
  }

  create(office: any) {
    return axios
    .post(API.local() + 'office/create/' + user.staff.company_id, office
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
    }
    )
    .then(response => {
      return response.data;
    });
    // return http.post("/office/create", office);
  }

  // update(office: any) {
  //   return axios
  //     .put(API_URL + 'update', office,{ headers: authHeader() })
  //     .then(response => {
  //       return response.data;
  //     });
  // }

  update(id: string, office: any) {
    return axios
        .post(API.local() + 'office/update/'+ id, office
        ,{headers:{ 
          'Content-type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'accessToken':  user.accessToken, 
          'staff_id': user.staff.id 
        }
        }
      )
      .then(response => {
        return response.data;
      });
    // return http.put('/office/update/'+ id, office);
  }
  delete(id: string) {
    return axios
      .get(API.local() + 'office/delete/'+ id 
      ,{headers:{ 
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'accessToken':  user.accessToken, 
        'staff_id': user.staff.id 
      }
      }
    )
    .then(response => {
      return response.data;
    });
      // return http.delete('/office/delete/' + id);
  }



  
}

export default new OfficeService();
