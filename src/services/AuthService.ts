import axios from 'axios';
// import authHeader from '../services/auth-header';
// const API_URL = 'https://ratco.co.tz/api/auth/';

import AuthorizationStorage from "./AuthorizationService";
import API from './aPILink';
const API_URL = 'http://localhost:8000/api/auth/';
// const API_URL = 'http://rts.portal.ratco.co.tz/api/auth/';

class AuthService {

  
  async login(email: string, password: string) {
    var responseFactor;
    return axios
      .post(API_URL + 'signin', {
        email,
        password
      },
      {headers:{
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }}
      )
      .then(response => {
        responseFactor = response.data;
        console.log(responseFactor.data);
        if (responseFactor.data.staff.access_token_status == 'active') {
          AuthorizationStorage.privilegeDataStorage(responseFactor.data.roles, responseFactor.data.privileges);
          localStorage.setItem('user', JSON.stringify(responseFactor.data));
          localStorage.setItem('company', JSON.stringify(responseFactor.data));
          localStorage.setItem('role', JSON.stringify(responseFactor.data.role));
          console.log("Wow");
        }
        return responseFactor;
      });
  }

  updatePassword(email: string, entered_password: string, new_password: string){
    return axios
      .post(API_URL + 'update/password', {
        email,
        entered_password,
        new_password
      }
      ,{headers:{
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }}
      )
      .then(response => {
        return response.data;
      });
  }

  // logout() {
  //   localStorage.removeItem('user');
  // }

  register(username: string, email: string, password: string) {
    return axios.post(API_URL + 'signup', {
      username,
      email,
      password
    });
  }
}

export default new AuthService();
