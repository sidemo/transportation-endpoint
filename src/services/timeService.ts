import axios from 'axios';
import authHeader from './auth-header';
import http from '../http-common';
import API from './aPILink';

const API_URL = 'http://rts.portal.ratco.co.tz/api/';
const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");
class timeService {
    getYears() {

        return axios
          .get(API.local() + 'years/'+ user.staff.company_id 
          ,{headers:{ 
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'accessToken':  user.accessToken, 
            'staff_id': user.staff.id 
          }
          }
          )
          .then(response => {
            return response.data;
          });
        // return http.get("/horses");
    }

    getMonths() {

        return axios
          .get(API.local() + 'months/'+ user.staff.company_id 
          ,{headers:{ 
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'accessToken':  user.accessToken, 
            'staff_id': user.staff.id 
          }
          }
          )
          .then(response => {
            return response.data;
          });
        // return http.get("/horses");
    }
}

export default new timeService();
