import axios from 'axios';
import authHeader from './auth-header';
import API from './aPILink';

const API_URL = 'http://localhost:8080/api/test/';

const storedUser = localStorage.getItem('user');
const user = JSON.parse(storedUser ? storedUser : "");

class UserService {
  verify() {
    if(user.staff.status == "new"){
      return false;
    }else{
      return true;
    }
  }

  // getUserBoard() {
  //   return axios.get(API_URL + 'user', { headers: authHeader() });
  // }

  // getModeratorBoard() {
  //   return axios.get(API_URL + 'mod', { headers: authHeader() });
  // }

  // getAdminBoard() {
  //   return axios.get(API_URL + 'admin', { headers: authHeader() });
  // }
}

export default new UserService();