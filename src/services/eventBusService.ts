import Vue from 'vue';
import EventBus from 'vue-bus-ts';
Vue.use(EventBus);
const bus = new EventBus.Bus();

export default bus