import axios from "axios";
import authHeader from './services/auth-header';

export default axios.create({
  baseURL: "http://rts.portal.ratco.co.tz/api",
  headers: authHeader
});
